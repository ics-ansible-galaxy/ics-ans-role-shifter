# ics-ans-role-shifter

Ansible role to install Shifter.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-shifter
```

## License

BSD 2-clause
